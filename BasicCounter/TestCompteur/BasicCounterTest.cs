﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterLib;


namespace TestCompteur
{
    [TestClass]
    public class BasicCounterTest
    {
        [TestMethod]
        public void TestIncrement_Compteur()
        {
            BasicCounterClass valeur = new BasicCounterClass(0);
            valeur.Increment_Compteur();
            Assert.AreEqual(1, valeur.getValeur());

        }
        [TestMethod]
        public void TestDecrement_Compteur()
        {
            BasicCounterClass valeur = new BasicCounterClass(4);
            valeur.Decrement_Compteur();
            Assert.AreEqual(3, valeur.getValeur());
        }
        [TestMethod]
        public void RemiseAZero_Compteur()
        {
            BasicCounterClass valeur = new BasicCounterClass(9);
            valeur.RemiseAZero_Compteur();
            Assert.AreEqual(0, valeur.getValeur());
        }
    }
}
