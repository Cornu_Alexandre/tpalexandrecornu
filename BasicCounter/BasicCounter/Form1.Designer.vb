﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Decrement_Compteur = New System.Windows.Forms.Button()
        Me.Increment_Compteur = New System.Windows.Forms.Button()
        Me.RemiseAZero_Compteur = New System.Windows.Forms.Button()
        Me.Total_Résultat = New System.Windows.Forms.Label()
        Me.Total_Name = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Decrement_Compteur
        '
        Me.Decrement_Compteur.Location = New System.Drawing.Point(164, 206)
        Me.Decrement_Compteur.Name = "Decrement_Compteur"
        Me.Decrement_Compteur.Size = New System.Drawing.Size(75, 23)
        Me.Decrement_Compteur.TabIndex = 0
        Me.Decrement_Compteur.Text = "-"
        Me.Decrement_Compteur.UseVisualStyleBackColor = True
        '
        'Increment_Compteur
        '
        Me.Increment_Compteur.Location = New System.Drawing.Point(574, 205)
        Me.Increment_Compteur.Name = "Increment_Compteur"
        Me.Increment_Compteur.Size = New System.Drawing.Size(75, 23)
        Me.Increment_Compteur.TabIndex = 1
        Me.Increment_Compteur.Text = "+"
        Me.Increment_Compteur.UseVisualStyleBackColor = True
        '
        'RemiseAZero_Compteur
        '
        Me.RemiseAZero_Compteur.Location = New System.Drawing.Point(358, 323)
        Me.RemiseAZero_Compteur.Name = "RemiseAZero_Compteur"
        Me.RemiseAZero_Compteur.Size = New System.Drawing.Size(75, 23)
        Me.RemiseAZero_Compteur.TabIndex = 2
        Me.RemiseAZero_Compteur.Text = "RAZ"
        Me.RemiseAZero_Compteur.UseVisualStyleBackColor = True
        '
        'Total_Résultat
        '
        Me.Total_Résultat.AutoSize = True
        Me.Total_Résultat.Location = New System.Drawing.Point(393, 177)
        Me.Total_Résultat.Name = "Total_Résultat"
        Me.Total_Résultat.Size = New System.Drawing.Size(0, 13)
        Me.Total_Résultat.TabIndex = 3
        '
        'Total_Name
        '
        Me.Total_Name.AutoSize = True
        Me.Total_Name.Location = New System.Drawing.Point(396, 83)
        Me.Total_Name.Name = "Total_Name"
        Me.Total_Name.Size = New System.Drawing.Size(31, 13)
        Me.Total_Name.TabIndex = 4
        Me.Total_Name.Text = "Total"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Total_Name)
        Me.Controls.Add(Me.Total_Résultat)
        Me.Controls.Add(Me.RemiseAZero_Compteur)
        Me.Controls.Add(Me.Increment_Compteur)
        Me.Controls.Add(Me.Decrement_Compteur)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Decrement_Compteur As Button
    Friend WithEvents Increment_Compteur As Button
    Friend WithEvents RemiseAZero_Compteur As Button
    Friend WithEvents Total_Résultat As Label
    Friend WithEvents Total_Name As Label
End Class
