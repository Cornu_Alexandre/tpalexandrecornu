﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasicCounterLib;

namespace BasicCounterLib
{
    public  class BasicCounterClass
    {
        private int valeur;

        public BasicCounterClass(int valeur)
        {
            this.valeur = valeur;
        }
        public  void Increment_Compteur()
        {
             this.valeur =  valeur + 1;
        }

        public   void Decrement_Compteur()
        {
            this.valeur =  valeur - 1;
        }
        
        public  void RemiseAZero_Compteur()
        {
            this.valeur = 0;
        }

        public int getValeur()
        {
            return this.valeur;

        }
    }
}
